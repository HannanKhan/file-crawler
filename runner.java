import java.io.*;
import java.util.*;

public class runner
{
  public static void main(String [] args)
  {
    testContent();
    findTest();
  }
  
  public static void testContent()
  {
    System.out.println("#################################TEST STARTS#################################");
    Crawler crawler = new Crawler();
    String directory = "C:\\Users\\hannan\\Desktop\\python\\";
    List<File> files = crawler.crawl(directory, 5);
    String keyword = "from";
    
    System.out.println("\t\tTesting content search, running at directory : " + directory);
    System.out.println("Expected Result : C:\\Users\\hannan\\Desktop\\python\\LAB3DIP\\OUTPUT.txt");
    
    for(File file: files)
    {
      if(( file.getName().endsWith("txt")) && crawler.hasWord(file, keyword) > 0)
      {
        System.out.println(keyword + " found in : " + file.getAbsolutePath());
      }
    }
    System.out.println("#################################TEST ENDS#############################");
    System.out.println();
  }
  
  public static void findTest()
  {
    System.out.println("#################################TEST STARTS#################################");
    Crawler crawler = new Crawler();
    String directory = "C:\\Users\\hannan\\Desktop\\python\\";
    String fname = "OUTPUT.txt";
    
    System.out.println("\t\tTesting find by name, running at directory : " + directory);
    System.out.println("Expected Result : C:\\Users\\hannan\\Desktop\\python\\LAB3DIP\\OUTPUT.txt");
    
    if(crawler.findFile(directory, 5, fname))
    {
      System.out.println("Test Passed!"); 
    }
    else
    {
      System.out.println("Test Failed!"); 
    }
    System.out.println("#################################TEST ENDS#############################");
  }
}