#Introduction
The goal of this lab was to understand and implement a local file system crawler.
#Implementation
The code is written in Java and unit tests are done manually. There are two files i.e. Runner.java and Crawler.java. Crawler.java has real implementation of Class Crawler where Runner.java is used for testing purpose.
There are three functions of Crawler Class i.e. crawl, hasWord and findFile.
Method crawl crawls through given path up to given depth and returns list of Files under that directory and its subdirectories.
Method hasWord checks if given file has given word or not.
Method findFile uses crawl function and checks if the give file name exists in resultant list.
#Learning
Learned the basic implementation of file spider.